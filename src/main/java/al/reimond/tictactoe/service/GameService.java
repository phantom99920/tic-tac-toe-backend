package al.reimond.tictactoe.service;

import al.reimond.tictactoe.exception.GameNotFoundException;
import al.reimond.tictactoe.models.Game;
import al.reimond.tictactoe.models.Movement;
import al.reimond.tictactoe.repo.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class GameService {

    private final GameRepository repository;

    @Autowired
    public GameService(GameRepository repository){
        this.repository = repository;
    }

    public Game addGame(Game game) {
        game.setId(UUID.randomUUID().toString());
        game.setStatus("ongoing");
        return repository.save(game);
    }

//    public List <Game> getOngoingGame(){
//
//        List<Game> allGame = repository.findAll();
//        allGame.stream()
//                .filter(p -> p.getStatus().equals("ongoing"));
//        return allGame;
//    }

    public List<Movement> getMovements(String id){
        Game game = repository.findById(id).orElseThrow(() ->new GameNotFoundException("Game by id"+ id + "was not found"));
        return game.getMovements();
    }

    public void setMovement(String id,Movement movement) {
        Game game = repository.findById(id).orElseThrow(() ->new GameNotFoundException("Game by id"+ id + "was not found"));
        game.setMovements(List.of(movement));
        repository.save(game);
    }

    public void addMovement(String id, Movement movement){
        Game game = repository.findById(id).orElseThrow(() ->new GameNotFoundException("Game by id"+ id + "was not found"));
        game.insertMovement(movement);
        repository.save(game);
    }

    public List<Game> findAllGames() {
        return repository.findAll();
    }

    public Game findGameById(String id){
       return repository.findById(id).orElseThrow(() ->new GameNotFoundException("Game by id"+ id + "was not found"));

    }

    public void deleteGames(String id) {
        repository.deleteGamesById(id);
    }

    public void endGame(String id){
        Game game = repository.findById(id).orElseThrow(()->new GameNotFoundException("Game by id"+ id+"was not found"));
        game.setStatus("finished");
        repository.save(game);
    }

    public void deleteAll() {repository.deleteAll();}


}
